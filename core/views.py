from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from account.models import User, UserToolRelation
from .models import Tool, TradeLog
from .forms import SendToolForm, SendMoneyForm


def top(request):
    return render(request, 'core/top.html', {'top': True})


@login_required
def send_item(request):
    form = SendToolForm(initial={'recipient': request.GET.get('r')})

    tool_choices = []
    for tool_rel in request.user.tool_relation.all():
        t = (str(tool_rel.tool.id), '{}(所持数:{})'.format(tool_rel.tool.name, str(tool_rel.count)))
        tool_choices.append(t)
    form.fields['tool'].choices = tool_choices

    if request.method == 'POST':
        form = SendToolForm(request.POST)
        form.fields['tool'].choices = tool_choices

        if form.is_valid():

            # 送信先ユーザーが存在するかどうか
            recipient = None
            try:
                recipient = User.objects.get(username=request.POST['recipient'])
                if recipient == request.user:
                    form.add_error('recipient', '自分自身には送信できません')
            except:
                form.add_error('recipient', '未登録のユーザー名です')

            sending_tool_id = int(request.POST.get('tool'))
            sending_tool_count = int(request.POST.get('tool_count'))

            # ツールをせずに数を指定すればエラー
            if sending_tool_count and not sending_tool_id:
                form.add_error('tool', 'ツールが未指定です')

            # ツールの指定数が所持数を超えているかどうか
            sender_tool_rel, sending_tool = None, None
            if sending_tool_id:
                sending_tool = Tool.objects.get(id=sending_tool_id)
                sender_tool_rel = UserToolRelation.objects.get(user=request.user, tool=sending_tool)

                # ツールを指定した場合はツールの数の指定が必須
                if not sending_tool_count:
                    form.add_error('tool_count', 'ツールを指定した場合は1以上でなければなりません')

                # 指定したツールの所持数を確認
                if sending_tool_count > sender_tool_rel.count:
                    form.add_error('tool_count', '所持数を超えています')

            # フォームに問題がなければ処理を実行
            if not form.errors:

                # 持ち主のrelから所持数を引く
                sender_tool_rel.count -= sending_tool_count
                if sender_tool_rel.count <= 0:
                    sender_tool_rel.delete()
                else:
                    sender_tool_rel.save()

                # 受けてのrelを作るか、増やす
                recip_tool_rel = UserToolRelation.objects.filter(user=recipient, tool=sending_tool)
                if not recip_tool_rel.exists():
                    UserToolRelation.objects.create(
                        user=recipient,
                        tool=sending_tool,
                        count=sending_tool_count
                    )
                else:
                    recip_tool_rel = recip_tool_rel.first()
                    recip_tool_rel.count += sending_tool_count
                    recip_tool_rel.save()

                TradeLog.objects.create(
                    sender=request.user,
                    recipient=recipient,
                    item=sending_tool,
                    count=sending_tool_count
                )
                messages.success(request, '送信完了！')
                return redirect(f'/form/tool/?r={recipient.username}')

    return render(request, 'core/generic_form.html',
                  {'form': form,
                   'title': 'アイテムを送る'})


def send_money(request):
    form = SendMoneyForm(initial={'recipient': request.GET.get('r')})
    form.fields['money'].label = f'マネー(残高: {request.user.money}円)'

    if request.method == 'POST':
        form = SendMoneyForm(request.POST)
        form.fields['money'].label = f'マネー{request.user.money}'

        if form.is_valid():

            # 送信先ユーザーが存在するかどうか
            recipient = None
            try:
                recipient = User.objects.get(username=request.POST['recipient'])
                if recipient == request.user:
                    form.add_error('recipient', '自分自身には送信できません')
            except:
                form.add_error('recipient', '未登録のユーザー名です')

            # 所持金より多い金額を設定していないかどうか
            sending_money = int(request.POST['money'])
            if sending_money > request.user.money:
                form.add_error('money', '指定金額が所持金額を上回っています')

            if not form.errors:
                request.user.money -= sending_money
                request.user.save()

                recipient.money += sending_money
                recipient.save()

                TradeLog.objects.create(
                    sender=request.user,
                    recipient=recipient,
                    count=sending_money
                )
                messages.success(request, '送信完了！')
                return redirect(f'/form/money/?r={recipient.username}')

    return render(request, 'core/generic_form.html',
                  {'form': form,
                   'title': 'マネーを送る'})
