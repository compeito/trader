from django.urls import path

from . import views
from account.views import users as views_user

app_name = 'core'
urlpatterns = [
    path('', views.top),
    path('send/tool/', views.send_item, name='tool'),
    path('send/money/', views.send_money, name='money'),
    path('<slug:username>/', views_user, name='users'),
]
