from django.db import models
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from core.utils import gen_id_str


def trade_id_str():
    return gen_id_str(8)


class Tool(models.Model):
    """
    生産用ツールモデル
    name: 表示名
    velocity: 生産速度
    price: 販売価格
    """
    name = models.CharField(max_length=255)
    velocity = models.FloatField(default=0)
    price = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name


class Product(models.Model):
    """
    生産されるモデル
    name: 表示名
    price: 換金価格
    """
    name = models.CharField(max_length=255)
    price = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name


class Team(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class TradeLog(models.Model):
    sender = models.ForeignKey('account.User', on_delete=models.SET_NULL,
                               related_name='sent_logs', null=True, blank=True)
    recipient = models.ForeignKey('account.User', on_delete=models.SET_NULL,
                                  related_name='received_logs', null=True, blank=True)

    item_type = models.ForeignKey(ContentType, on_delete=models.SET_NULL, null=True, blank=True)
    item_id = models.PositiveIntegerField(null=True, blank=True)
    item = GenericForeignKey('item_type', 'item_id')

    count = models.PositiveIntegerField()

    created_at = models.DateTimeField(default=timezone.now)

