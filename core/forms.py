from django import forms


class SendToolForm(forms.Form):
    recipient = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': '例:TwitterJP'}),
        max_length=15,
        label='取引相手',
        help_text='ユーザーID(スクリーンネーム)で記述。@不要',
        required=True
    )
    tool = forms.ChoiceField(
        label='ツール',
        required=True
    )
    tool_count = forms.IntegerField(
        widget=forms.NumberInput(attrs={'value': 1}),
        min_value=1,
        label='ツールの数',
        help_text='所持数以上を設定した場合エラーを返します',
        required=True
    )


class SendMoneyForm(forms.Form):
    recipient = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': '例:TwitterJP'}),
        max_length=15,
        label='取引相手',
        help_text='ユーザーID(スクリーンネーム)で記述。@不要',
        required=True
    )
    money = forms.IntegerField(
        widget=forms.NumberInput(attrs={'value': 100}),
        min_value=100,
        label='マネー',
        help_text='100~所持金以上を設定した場合エラーを返します',
        required=True
    )
