from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from core.utils import get_user_api
from .models import User
from .forms import UserSettingsForm


@login_required
def login_after_redirect(request):
    user = request.user
    if not user.data_imported:
        try:
            api = get_user_api(request.user)
            user.json = api.VerifyCredentials().AsDict()
            user.data_imported = True
            user.save()
        except:
            return redirect('/')

    return redirect('/')


def users(request, username):
    account = get_object_or_404(User, username=username)
    return render(request, 'account/page.html', {'account': account})


@login_required
def user_settings(request):
    user = request.user
    if request.method == 'POST':
        form = UserSettingsForm(request.POST, request.FILES, instance=user)

        if not user.username == request.POST['username']:
            form.add_error('username', 'ユーザー名は変更できません')
            return render(request, 'account/settings.html', {'form': form})

        if form.is_valid():
            form.save()
            messages.success(request, '変更が完了しました')
            return render(request, 'account/settings.html', {'form': form})

    form = UserSettingsForm(instance=user)
    return render(request, 'account/settings.html', {'form': form})
