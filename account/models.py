from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager

import jsonfield

import os
from uuid import uuid4


def profile_icon_upload_to(instance, filename):
    return os.path.join('users', str(instance.username), 'profile_icon', uuid4().hex + '.jpg')


class User(AbstractUser):
    # twitter
    json = jsonfield.JSONField()
    data_imported = models.BooleanField(default=False)

    money = models.PositiveIntegerField(default=0)
    team = models.ForeignKey('core.Team', on_delete=models.SET_NULL, null=True, blank=True)

    tools = models.ManyToManyField('core.Tool', through='account.UserToolRelation')

    objects = UserManager()

    class Meta(object):
        app_label = 'account'


class UserToolRelation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tool_relation')
    tool = models.ForeignKey('core.Tool', on_delete=models.CASCADE, related_name='user_relation')
    count = models.PositiveIntegerField(default=1)
